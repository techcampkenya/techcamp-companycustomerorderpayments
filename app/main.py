from flask import Flask,session,render_template,jsonify,session
from flask_sqlalchemy import SQLAlchemy
import datetime
from flask_marshmallow import Marshmallow
from configs.configs import Development, Staging, Production
import psycopg2

app = Flask(__name__)

app.config['PROPAGATE_EXCEPTIONS'] = True
#config to use when using localhost
# app.config.from_object(Development)
#config to use when production || 
app.config.from_object(Production)

def create_app():
    app = Flask(__name__)
    return app

db = SQLAlchemy(app)
ma = Marshmallow(app)


from models.orders import OrdersModel
from models.crm import CRMModel,CRMSchema
from models.materials import MaterialsModel
from models.payment import PaymentModel 
from models.customers import CustomerModel
from models.items import ItemsModel,items_schema
from models.company import CompanyModel
from models.expcategory import ExpenditureCategory
from models.expenditure import ExpenditureModel
from models.incategory import IncomeCategory
from models.income import IncomeModel
from models.delivery import DeliveryModel
from models.email import EmailModel
from models.orderDetails import OrderDetails,od_schema


@app.before_request
def before_request():
    session.permanent = True
    app.permanent_session_lifetime = datetime.timedelta(minutes=30)


@app.before_first_request
def create_tables():
    db.create_all()
    db.create_all(bind=['customers'])
    db.create_all(bind=['orders'])
    db.create_all(bind=['payments'])
    db.create_all(bind=['company'])
    db.create_all(bind=['income'])
    db.create_all(bind=['expenditure'])
    db.create_all(bind=['email'])
    # db.drop_all(bind=['orders'])
    # db.drop_all()
    # db.drop_all(bind='__all__')



from views.decoratorsController import *
from views.authController import *
from views.companyController import *
from views.dashboardContoller import *
from views.customerController import *
from views.emailController import *
from views.incomeExpeController import *
from views.orderController import *
from views.paymentsController import *
from views.rolesController import *
from views.tasksController import *



if __name__ == '__main__':
    app.run(host='0.0.0.0')
