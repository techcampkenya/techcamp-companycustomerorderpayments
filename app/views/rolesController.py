############################################################################################################
""" Bizweb add roles api """
############################################################################################################
from flask import request,redirect,render_template,url_for,flash,session,jsonify

from main import app
from views.decoratorsController import *
from models.users import *


a_role = RoleSchema()
roles = RoleSchema(many=True)

@app.route('/bizweb/roles', methods=['POST'])
def bizweb_roles():
    if request.is_json:
        data = request.get_json(force=True)
        name = data['name']
        description = data['description']

        record = RolesModel(name=name,description=description)
        save = record.create_record()
        return a_role.dump(save), 201
    else:
        return jsonify({'message':'JSON request expected!'}), 400


@app.route('/bizweb/roles', methods=['GET'])
def biz_roles():
    records = RolesModel.fetch_records()
    return jsonify({"roles":roles.dump(records)}), 200

# 4452 - jose
# 3632 - peter
# 7523 - Eduh