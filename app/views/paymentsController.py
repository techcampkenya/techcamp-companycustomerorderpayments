from flask import request,redirect,render_template,url_for,flash,session

from main import app
from views.decoratorsController import *

from models.payment import PaymentModel 


# all payments
@app.route('/payments', methods=['GET','POST'])
@user_login_required
@payments_role_required
def payments():
    if session:
        
        company_name = session['company_name']
        company_id = session['company_id']
        username = session['user_name']

        allpayments = PaymentModel.get_payment_by_companyId(company_id)
        return render_template('payments.html', allpayments=allpayments,
                            user=username, companyName=company_name)

    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))

