from flask import request,redirect,render_template,url_for,flash,session

from main import app
from views.decoratorsController import *
from models.customers import CustomerModel
from controllers.customersController import Customer
cus = Customer()
from controllers.ordersController import Orders
ordr = Orders()


# Customers
@app.route('/customers', methods=['GET', 'POST'])
@user_login_required
@customer_role_required
def customers():
    if session:
        company_name = session['company_name']
        company_id = session['company_id']
        username = session['user_name']
        # print(company_id)

        all_customers = CustomerModel.get_customer_by_company(company_id)
        # print(all_customers)

        
        if request.method == 'POST':
            cus.register_customer()
            return redirect(url_for('customers'))

        return render_template('customers.html',all=all_customers, user=username, companyName=company_name,
                            cid=company_id)
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))

# ADD NEW CUSTOMER
@app.route('/addcustomer', methods=['GET','POST'])
@user_login_required
@customer_role_required
def add_customer():
    if session:
        company_name = session['company_name']
        company_id = session['company_id']
        username = session['user_name']
        
        if request.method == 'POST':
            cus.register_customer()
            return redirect(url_for('add_customers'))

        return render_template('addcustomer.html',user=username, companyName=company_name,
                            cid=company_id)
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))



# edit customer
@app.route('/customer/<int:id>/edit', methods=['GET','POST'])
@user_login_required
@customer_role_required
def edit_customer_record(id):
    if session:
        company_name = session['company_name']
        company_id = session['company_id']
        username = session['user_name']

        if request.method == 'POST':
            cus.update_customer_records(id)
            return redirect(url_for('customers'))

        return redirect(url_for('customers'))
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))


# delete a customer record
@app.route('/customer/<int:id>/delete', methods=['GET','POST'])
@user_login_required
@customer_role_required
def delete_customer_record(id):
    if session:
        company_name = session['company_name']
        company_id = session['company_id']
        username = session['user_name']

        if request.method == 'POST':
            cus.delete_customer_record(id)
            # send_message("deleted customer id {} record".format(id),username,session['user_id'],company_id)
            return redirect(url_for('customers'))
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))


# A customer order
@app.route('/manage/<int:id>/customer', methods=['GET','POST'])
@user_login_required
def customer_order(id):
    if session:
        company_name = session['company_name']
        company_id = session['company_id']
        username = session['user_name']

        # get the customer using the id
        customa = CustomerModel.get_customer_by_id(id)
        # get the orders associated to that customer
        customa_orders = ordr.get_customer_orders(id)

        if request.method == 'POST':
            ordr.add_order()
            return redirect(url_for('customer_order', id=id))

        return render_template('customer.html', name=customa.full_name,custo_id=id, orders=customa_orders,
                            user=username, companyName=company_name,company_id=company_id,
                            useroles=session['roles'] )
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))


