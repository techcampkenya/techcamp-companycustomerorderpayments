from flask import request,redirect,render_template,url_for,flash,session
from main import app,psycopg2
from views.decoratorsController import *

from controllers.userController import User
usr = User()
#INCOME EXPENSE controllers
from controllers.companyController import Company
com1 = Company()
from controllers.userController import User
user1 = User()

from models.company import CompanyModel
from models.users import *


# home page
@app.route('/home')
@user_login_required
@income_expense_role_required
def home():
    if session:
        
        company_name = session['company_name']
        company_id = session['company_id']
        user_name = session['user_name']

        # connect to an existing database
        # conn = psycopg2.connect(" dbname='appIncomeDatabase' user='postgres' host='localhost' password='Brian8053@' ")
        conn = psycopg2.connect(" dbname='omsincome' user='postgres' host='172.17.0.1' password='Brian8053@' ")

        # Open a cursor to perform database operations
        cur = conn.cursor()

        # Execute a command: this gets the sum of all income made per month!
        cur.execute(" SELECT SUM(amount),to_char(to_timestamp(EXTRACT(MONTH FROM incomes.created_on)::text,'MM'),'Mon') as income_month FROM incomes WHERE incomes.company_id={} GROUP BY income_month;".format(company_id))
        # all rows
        all_rows = cur.fetchall()
        print("Money rows", all_rows)
        income_amounts = []
        income_dates = []
        for income in all_rows:
            income_amounts.append(income[0])
            income_dates.append(income[1])
        

        # Connect to the expennse db
        # conn2 = psycopg2.connect(" dbname='appExpenditureDatabase' user='postgres' host='localhost' password='Brian8053@' ")
        conn2 = psycopg2.connect(" dbname='omsexpenditure' user='postgres' host='172.17.0.1' password='Brian8053@' ")

        # Open a cursor to perform database operations
        cur2 = conn2.cursor()
        # Execute a command: this gets the sum of all income made per month!
        cur2.execute("select sum(amount), to_char(to_timestamp(extract(month from expenditures.created_on)::text,'MM'),'Mon') as exp_month from expenditures where expenditures.company_id={} group by exp_month;".format(company_id))
        all_rows2 = cur2.fetchall()
        amounts = []
        months = []
        for month_summary in all_rows2:
            amounts.append(month_summary[0])
            months.append(month_summary[1])

        return render_template('index.html',company=company_name, user=user_name,months=months,amounts=amounts,
                                income_dates=income_dates,income_amounts=income_amounts)
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))

# select sum(amount), to_char(to_timestamp(extract(month from expenditures.created_on)::text,'MM'),'Mon') as exp_month from expenditures where expenditures.company_id=2 group by exp_month;



# ADD USER
@app.route('/adduser')
@admin_role_required
@user_login_required
def add_user():
    if session:
    
        company_id = session['company_id']
        company_name = session['company_name']
        user_name = session['user_name']

        # get the company that is logged in
        company = CompanyModel.get_company_by_id(company_id)
        
        # get the users that are in a particular company
        company_users = company.users
        # print("type is", company_users)
        
        if request.method == 'POST':
            usr.register_user()
            return redirect(url_for('user_roles'))

        return render_template('adduser.html',company_id=company_id,company=company_name,user=user_name)
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))


@app.route('/userroles',methods=['GET','POST'])
@user_login_required
@admin_role_required
def user_roles():
    if session:
    
        company_id = session['company_id']
        company_name = session['company_name']
        user_name = session['user_name']

        # get the company that is logged in
        company = CompanyModel.get_company_by_id(company_id)
        
        # get the users that are in a particular company
        company_users = company.users
        # print("type is", company_users)

        if request.method == 'POST':
            usr.register_user()
            return redirect(url_for('user_roles'))

        return render_template('userroles.html',company_id=company_id,company=company_name,user=user_name,
                            users=company_users)
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))


# edit user details
@app.route('/edit/<int:id>/user', methods=['GET','POST'])
@user_login_required
@admin_role_required
def edit_user_details(id):
    if session:
        company_id = session['company_id']
        company_name = session['company_name']
        user_name = session['user_name']

        if request.method == 'POST':
            usr.edit_user_details(id)
            return redirect(url_for('user_roles'))
        return redirect(url_for('user_roles'))
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))


@app.route('/assign/<int:id>/role', methods=['GET','POST'])
@admin_role_required
def assign_roles(id):
    if session:
        company_id = session['company_id']
        company_name = session['company_name']
        user_name = session['user_name']

        # get all the roles
        the_roles = RolesModel.fetch_records()

        # get user by id
        useeer = UserModel.get_user_byID(id)

        # get all the user roles
        current_user_roles = useeer.role

        if request.method == 'POST':
            role_id = request.form['role']

            # get role by id
            rolee = RolesModel.get_role_byID(role_id)

            # assign the user the role
            useeer.role.append(rolee)
            db.session.commit()
            flash('Role has successfully been added!','success')
            # send_message("assigned new user role",username,session['user_id'],company_id)
            return redirect(request.url)
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))


    return render_template('viewroles.html',roles=the_roles, uroles=current_user_roles, user_id=id)


# delete a role
@app.route('/user/<int:userid>/delete/<int:roleid>', methods=['GET','POST'])
@admin_role_required
def delete_user_role(userid,roleid):
    if session:
        company_id = session['company_id']
        company_name = session['company_name']
        user_name = session['user_name']

        # get user by id
        user = UserModel.get_user_byID(userid)
        # get role by id
        rolee = RolesModel.get_role_byID(roleid)

        if request.method == 'POST':
            # delete the role
            user.role.remove(rolee)
            db.session.commit()
            flash('Role has successfully been deleted!', 'success')
            # send_message("deleted user role",username,session['user_id'],company_id)
            return redirect(url_for('assign_roles', id=userid)) 
        return redirect(url_for('assign_roles', id=userid)) 
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))


# suspend a user   
@app.route('/suspend/<int:id>/user', methods=['POST'])
@user_login_required
@admin_role_required
def suspend_user(id):
    if session:
        if request.method == 'POST':
            UserModel.suspend_user(id)
            flash('User id {} account has successfully been blocked!'.format(id), 'success')
            # send_message("blocked user account",username,session['user_id'],company_id)
            return redirect(url_for('user_roles'))
            
        return redirect(url_for('user_roles'))
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))


@app.route('/reactivate/<int:id>/user', methods=['POST'])
@user_login_required
@admin_role_required
def reactivate_user(id):
    if session:
        if request.method == 'POST':
            UserModel.reactivate_user(id)
            flash('User id {} account has successfully been unblocked!'.format(id), 'success')
            # send_message("activated user account",username,session['user_id'],company_id)
            return redirect(url_for('user_roles'))
            
        return redirect(url_for('user_roles'))
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))


# USER RESET PASSWORD
@app.route('/user/resetpassword', methods=['GET','POST'])
def user_resetpassword():
    return render_template('userrestpassword.html')

# testuser@gmail.com pass 4500