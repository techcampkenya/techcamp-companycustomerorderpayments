from flask import request,redirect,render_template,url_for,flash,session

from main import app,psycopg2
from views.decoratorsController import *
from models.orders import OrdersModel
from models.orderDetails import OrderDetails, od_schema
from models.payment import PaymentModel 
from models.users import UserModel
from models.items import ItemsModel,items_schema
from controllers.ordersController import Orders
ordr = Orders()
from controllers.crmController import CRM
crm = CRM()
from controllers.deliveryController import Delivery
delvry = Delivery()
from controllers.paymentController import Payment
pay = Payment()

from models.customers import CustomerModel

# edit an order
@app.route('/customer/<int:cid>/order/<int:id>/edit', methods=['GET','POST'])
@user_login_required
@customer_role_required
def edit_order(id,cid):
    if session:
        company_name = session['company_name']
        company_id = session['company_id']
        username = session['user_name']

        if request.method == 'POST':
            ordr.edit_order_details(id)
            return redirect(url_for('customer_order', id=cid))

        return redirect(url_for('customer_order', id=cid))
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))



# an order item(s)
@app.route('/order/<int:id>/management', methods=['GET','POST'])
@user_login_required
def order(id):
    if session:
        company_name = session['company_name']
        company_id = session['company_id']
        username = session['user_name']


        # get an order by the id
        order = OrdersModel.get_order_byId(id)
        # GET order status
        order_delivery_status = order.delivery_status
        # GET all the order items
        order_items = order.order_details

        # get the number of items in the order
        number_of_items = len(order_items)

        # order deliveries
        delivery = order.deliveries
        # print(delivery)

        # print([x.item_cost for x in order_items])
        # CALCULATE the order total
        order_total = sum([x.sub_total for x in order_items])
        # GET order crm
        order_crm = order.crms
        # GET order materials
        ordermaterials = order.materials
        # GET customer associated with the order
        customer_id = OrdersModel.get_customerid(id)
        customer = CustomerModel.get_customer_by_id(customer_id)
        # order payments
        all_order_payments = PaymentModel.get_payments_by_orderId(id)
        # calculate the total amount paid to an order
        payments_total = sum([y.amount for y in all_order_payments])

        if request.method == 'POST':
            ite.add_item()
            return redirect(request.url)

         # Connect to an existing database
        # conn = psycopg2.connect(" dbname='varicciOrders' user='postgres' host='localhost' password='Brian8053@' ")
        conn = psycopg2.connect(" dbname='omsorders' user='postgres' host='172.17.0.1' password='Brian8053@' ")

        # Open a cursor to perform database operations
        cur = conn.cursor()

        # Execute a command: fetch total number of orders made in a day
        cur.execute("select i.title, i.unit_cost,od.quantity,od.sub_total from items as i join order_details as od on i.id=od.item_id where od.company_id={}".format(company_id))
        rows = cur.fetchall() 
        
        return render_template('order.html',order_id=id,order=order, 
                                items=rows, crms=order_crm, cname=customer, 
                                total=order_total,materials=ordermaterials,payments=all_order_payments, 
                                paid=payments_total, ds=order_delivery_status,
                                user=username, companyName=company_name,cid=company_id,
                                number=number_of_items,delivery=delivery,
                                useroles=session['roles']) 
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))



# add new company item
@app.route('/additem', methods=['GET','POST'])
@admin_role_required
@user_login_required
def company_items():
    if session:
        company_name = session['company_name']
        company_id = session['company_id']
        username = session['user_name']
        user_email = session['user_email'] 
        userId = UserModel.get_user_id(user_email)

        if request.method == 'POST':
            title = request.form['title']
            description = request.form['description']
            unit_cost = request.form['unit_cost']
            
            record = ItemsModel(title=title,description=description,company_id=company_id,
                                user_id=userId,unit_cost=unit_cost)
            record.insert_record()
            flash('Item successfully added!','success')
            return redirect(url_for('get_company_items'))

        return render_template('addCompItems.html',cid=company_id,user_id=userId)
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))


@app.route('/viewitems', methods=['GET','POST'])
@admin_role_required
@user_login_required
def get_company_items():
    if session:
        company_name = session['company_name']
        company_id = session['company_id']
        username = session['user_name']
        user_email = session['user_email'] 
        userId = UserModel.get_user_id(user_email)

        all_company_items = ItemsModel.fetch_all_records()

        return render_template('viewCompItems.html',companyName=company_name,
                            user=username,all_items=all_company_items)
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))

# json itmes
@app.route('/items/json', methods=['GET','POST'])
def json_item():
    if session:
        company_name = session['company_name']
        company_id = session['company_id']
        username = session['user_name']
        user_email = session['user_email'] 
        userId = UserModel.get_user_id(user_email)

        all_items = ItemsModel.get_items_by_companyId(company_id)
        
        if request.method == 'POST':
            data = request.get_json(force=True)
            print(data)
            record = OrderDetails(order_id=data['order_id'],item_id=data['item_id'],company_id=data['company_id'],
                                    quantity=data['quantity'],sub_total=data['subtotal'])
            detail = record.insert_record()
            return od_schema.dumps(detail)


        return items_schema.jsonify(all_items)



# a function to convert a tuple to a dict
def convert_to_dict(item):
    the_dict = {
        "title":item[0],
        "unit_cost":item[1],
        "quantity":item[2],
        "sub_total":item[3]
    }
    return the_dict

# json order details
@app.route('/orderitems/<int:id>/details')
@user_login_required
def json_orderitems(id):
    if session:
        company_name = session['company_name']
        company_id = session['company_id']
        username = session['user_name']
        user_email = session['user_email'] 
        userId = UserModel.get_user_id(user_email)
        
        # Connect to an existing database
        # conn = psycopg2.connect(" dbname='varicciOrders' user='postgres' host='localhost' password='Brian8053@' ")
        conn = psycopg2.connect(" dbname='omsorders' user='postgres' host='172.17.0.1' password='Brian8053@' ")

        # Open a cursor to perform database operations
        cur = conn.cursor()

        # Execute a command: fetch total number of orders made in a day
        cur.execute("select i.title, i.unit_cost,od.quantity,od.sub_total,od.order_id from items as i join order_details as od on i.id=od.item_id where od.company_id={} and od.order_id={}".format(company_id,id))
        rows = cur.fetchall() 
        # select i.title, i.unit_cost,od.quantity,od.sub_total,od.order_id  from items as i join order_details as od on i.id=od.item_id where od.company_id=1;
        res = map(convert_to_dict, rows)
        # print(list(res))
        import json
        x = json.dumps(list(res))
    
        return x


# send an invoice
@app.route('/send/<int:id>/invoice', methods=['GET','POST'])
def send_invoice(id):
    if session:
        if request.method == 'POST':
            company_name = session['company_name']
            company_id = session['company_id']
            username = session['user_name']
            user_email = session['user_email'] 
            userId = UserModel.get_user_id(user_email)
            print(userId)

            company = CompanyModel.query.filter_by(id=company_id).first()
            cemail = company.company_email

            # get an order by the id
            order = OrdersModel.get_order_byId(id)
            # GET order status
            order_delivery_status = order.delivery_status
            # GET order items
            order_items = order.items

            # get the number of items in the order
            number_of_items = len(order_items)

            # order deliveries
            delivery = order.deliveries
            # print(delivery)

            # print([x.item_cost for x in order_items])
            # CALCULATE the order total
            order_total = sum([x.item_cost for x in order_items])
            # GET order crm
            order_crm = order.crms
            # GET order materials
            ordermaterials = order.materials
            # GET customer associated with the order
            customer_id = OrdersModel.get_customerid(id)
            customer = CustomerModel.get_customer_by_id(customer_id)
            # order payments
            all_order_payments = PaymentModel.get_payments_by_orderId(id)
            # calculate the total amount paid to an order
            payments_total = sum([y.amount for y in all_order_payments])

            import datetime
            today = datetime.datetime.now().strftime("%Y-%m-%d")
            
            try:
                # generate the pdf
                invoice =  render_template('invoicetemplate.html',order_id=id,order=order, 
                                        items=order_items,cname=customer, 
                                        total=order_total,materials=ordermaterials,payments=all_order_payments, 
                                        paid=payments_total, ds=order_delivery_status,
                                        user=username, companyName=company_name,cid=company_id,
                                        number=number_of_items,delivery=delivery, today=today, email=cemail,
                                        useroles=session['roles']) 
                
                subj = request.form['subject']
                from configs.configs import send_simple_email
                x = send_simple_email(customer.email,subj,invoice,company_id)
                flash('Invoice has successfully been sent', 'success')
                # send_message("sent order invoice",username,session['user_id'],company_id)
                # 0 - represents an invoice
                email = EmailModel(company_id=company_id,user_id=userId,customer_id=customer_id,
                                    subject=subj,content=invoice, email_type=0,delivery_status=0,read_status=0)
                email.insert_record()
            except Exception as e:
                print(e)
                flash("Could not send email at this time, try again later!", 'danger')
    
        return redirect(url_for('order',id=id))



# an order crm
@app.route('/order/<int:id>/crm', methods=['GET', 'POST'])
@user_login_required
def order_crm(id):
    if session:
        
        if request.method == 'POST':
            crm.add_crm()
            return redirect(url_for('order', id=id))

        return redirect(url_for('order', id=id))
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))

# an order material(s)
@app.route('/order/<int:id>/material', methods=['POST'])
@user_login_required
def order_material(id):
    if session:
        if request.method == 'POST':
            mat.add_material()
            return redirect(url_for('order', id=id))

        return redirect(url_for('order', id=id))
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))
    
# an order payment
@app.route('/order/<int:id>/payment', methods=['POST'])
@user_login_required
def order_payment(id):
    if session:
        company_name = session['company_name']
        company_id = session['company_id']
        username = session['user_name']

        # get the order by the id, then get items in that order.
        order =  OrdersModel.get_order_byId(id)
        order_items = order.order_details
        # check the number of items in the order, if the  number == 0: disable the payment.
        number_of_items = len(order_items)

        if request.method == 'POST':
            pay.add_payment()
            # send_message("added manual order payment",username,session['user_id'],company_id)
            return redirect(url_for('order', id=id))

        return redirect(url_for('order', id=id))
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))

# update order delivery status to in transit
@app.route('/order/<int:id>/intrasit', methods=['POST'])
@user_login_required
def order_intransit(id):
    if session:
        if request.method == 'POST':
            OrdersModel.update_status_to_intransit(id)
            # send_message("updated order status",username,session['user_id'],company_id)
            return redirect(url_for('order', id=id))
        return redirect(url_for('order', id=id))
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))
    
# update order delivery status to in transit
@app.route('/order/<int:id>/delivered', methods=['POST'])
@user_login_required
def order_delivered(id):
    if session:
        if request.method == 'POST':
            OrdersModel.update_status_to_delivered(id)
            # send_message("updated order status",username,session['user_id'],company_id)
            return redirect(url_for('order', id=id))
        return redirect(url_for('order', id=id))
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))



@app.route('/delivery/<int:id>/order', methods=['POST'])
@user_login_required
def delivery(id):
    if session:
        if request.method == 'POST':
            delvry.add_delivery()
            # send_message("added order delivery details",username,session['user_id'],company_id)
            return redirect(url_for('order', id=id))
        return redirect(url_for('order', id=id))
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))



# all orders
@app.route('/orders', methods=['GET','POST'])
@user_login_required
@orders_role_required
def orders():
    if session:
        
        company_name = session['company_name']
        company_id = session['company_id']
        username = session['user_name']

        allorders = OrdersModel.get_orders_byCompanyId(company_id)
        paidList = []
        for each in allorders:
            x=PaymentModel.get_payments_by_orderId(each.id)
            a = map(lambda i: i.amount, x)
            paidList.append(sum(list(a)))
        # print(paidList)

        itemsTotal = []
        for every in allorders:
            items = OrdersModel.get_items(every.id)
            a = map(lambda x: x.sub_total,items)
            itemsTotal.append(sum(list(a)))
        
        # print(itemsTotal)

        return render_template('allorders.html', orders=allorders, totalPaid=paidList ,itemstotal=itemsTotal,
                            user=username, companyName=company_name)
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))


@app.route('/order/<int:oid>/crm/<int:id>/update', methods=['GET','POST'])
@user_login_required
def crm_update(oid,id):
    if session:
        if request.method == 'POST':
            CRMModel.update_crm_to_complete(id)
            # send_message("updated crm status",username,session['user_id'],company_id)
            return redirect(url_for('order',id=oid))
