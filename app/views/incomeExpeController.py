from flask import request,redirect,render_template,url_for,flash

from main import app,psycopg2
from views.decoratorsController import *


from controllers.expenditureController import Expenditure
exp1 = Expenditure()
from controllers.incomeController import Income
inc1 = Income()

# expenditure
@app.route('/expenditure', methods=['GET','POST'])
@user_login_required
@payments_role_required
def expenditure():
    if session:
        company_id = session['company_id']
        company_name = session['company_name']
        user_name = session['user_name']

        ecategories = [
            'Equipment Insurance',
            'Office Supplies',
            'Marketing, advertising, and promotion',
            'Salaries, benefits, and wages',
            'Rent',
            'Insurance',
            'Depreciation and amortization',
            'Taxes',
            'Travel and Meetings',
            'Worker Compensation Insurance',
            'others'
        ]

        expenditure_categories = exp1.get_exp_categories_by_company(company_id)
        expenditures = exp1.get_company_expenditures(company_id)

        if request.method == 'POST':
            exp1.add_expenditure()
            return redirect(url_for('expenditure'))

        return render_template('expenditure.html',company_id=company_id,
                            company=company_name, user=user_name ,categories=ecategories, 
                            expenditures=expenditures)
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))


# income page
@app.route('/income', methods=['GET', 'POST'])
@user_login_required
@payments_role_required
def income():
    if session:
        company_id = session['company_id']
        company_name = session['company_name']
        user_name = session['user_name']
        
        icategories = [
            'Payments'
        ]

        categories = inc1.get_company_income_categories(company_id)
        incomes = inc1.get_company_incomes(company_id)
        # print(incomes)

        if request.method == 'POST':
            inc1.add_income()
            return redirect(url_for('income'))
        return render_template('income.html',company_id=company_id,company=company_name,
                user=user_name, incomes=incomes,categories=icategories)
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))