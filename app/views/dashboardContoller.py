from flask import request,redirect,render_template,url_for,flash

from main import app,psycopg2
from views.decoratorsController import *
from models.customers import CustomerModel
from models.orders import OrdersModel
from models.crm import CRMModel,CRMSchema


@app.route('/')
@user_login_required
def dashboard():
    if session:
        company_name = session['company_name']
        company_id = session['company_id']
        username = session['user_name']

        # get total number of customers in the company
        total_customers = CustomerModel.get_customer_by_company(company_id)
        # get total number of orders in the company
        total_company_orders = OrdersModel.get_orders_byCompanyId(company_id)
        # get pending orders in the company
        pending_orders = [x.delivery_status for x in total_company_orders if x.delivery_status == 0]
        # get total number of delivered orders in the company
        delivered_orders = [x.delivery_status for x in total_company_orders if x.delivery_status == 2]
        
        # get incomplete tasks
        incomplete_tasks = CRMModel.get_crms_by_companyId(company_id)

        # Connect to an existing database
        # conn = psycopg2.connect(" dbname='varicciOrders' user='postgres' host='localhost' password='Brian8053@' ")
        conn = psycopg2.connect(" dbname='omsorders' user='postgres' host='172.17.0.1' password='Brian8053@' ")

        # Open a cursor to perform database operations
        cur = conn.cursor()

        # Execute a command: fetch total number of orders made in a day
        cur.execute("SELECT DATE(order_date) AS date, COUNT(id) FROM orders WHERE orders.company_id={}  GROUP BY date ORDER BY date DESC;".format(company_id))
        rows = cur.fetchall() 
       
        #create two empty lists, on to store all the dates, the other to store total-orders
        dates = []
        total_orders = []
        for each in rows:
            dates.append(each[0].isoformat()) #date return the following format datetime.date(2020, 2, 5) so format it in iso yyyy-mm-dd
            total_orders.append(each[1])
        
        
        # NOT PAID ORDERS
        not_paid_orders = [x.payment_status for x in total_company_orders if x.payment_status == 0]
        # print("Not paid", not_paid_orders)

        # PARTIALLY PAID ORDERS
        partially_paid_orders = [x.payment_status for x in total_company_orders if x.payment_status == 1]
        # print("Partially paid",partially_paid_orders)

        # FULLY PAID ORDERS
        fully_paid_orders =[x.payment_status for x in total_company_orders if x.payment_status == 2]
        # print("fully paid",fully_paid_orders)

        # Orders that are bookings
        bookings = [x.order_status for x in total_company_orders if x.order_status == 0]
        # print("bkings",bookings)
        # orders
        all_orders = [x.order_status for x in total_company_orders if x.order_status == 1]
        # print("orders",all_orders)

        # percentage of delivered orders
        tco = len(total_company_orders)
        if tco != 0:
            td0 = len(delivered_orders)
            pdo = (td0/tco) * 100
            
            # percentage of fully paid orders
            tfpo = len(fully_paid_orders)
            pfpo = (tfpo/tco) * 100

            # percentage of partially paid orders
            tppo = len(partially_paid_orders)
            pppo = (tppo/tco) * 100

            # percentage of not paid orders
            tnpo = len(not_paid_orders)
            pnpo = (tnpo/tco) * 100
        else:
            pdo = 0
            pfpo = 0
            pppo = 0
            pnpo = 0


        # tc-total customers, to-total orders, po-pending orders, do-delivered orders
        return render_template('dashboard.html', user=username, companyName=company_name,
                                tc=len(total_customers),to=len(total_company_orders),
                                po=len(pending_orders), incomplete_tasks=len(incomplete_tasks),
                                notpaid=len(not_paid_orders),
                                partially_paid=len(partially_paid_orders),fully_paid=len(fully_paid_orders),
                                bookings=len(bookings),all_orders=len(all_orders),
                                pdo=round(pdo),pfpo=round(pfpo,2),pppo=round(pppo,2), pnpo=round(pnpo,2),
                                useroles=session['roles'], orde=rows,
                                dates=dates,totalO=total_orders)
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))
