from main import app,jsonify
from flask import request, render_template, redirect,url_for,session, flash,make_response,json
from functools import wraps
import pdfkit
import os


# from producer.producer import send_message
###############################################################################################################
"""Models imports goes here"""

################################################################################################################
"""Controller imports goes here"""

# controllers and their objects/instances
from controllers.customersController import Customer
cus = Customer()

from controllers.itemsController import Item
ite = Item()
from controllers.crmController import CRM
crm = CRM()
from controllers.materialsController import Material
mat = Material()
from controllers.paymentController import Payment
pay = Payment()
from controllers.deliveryController import Delivery
delvry = Delivery()

###############################################################################################################
"""Sentry imports"""
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration
# sentry_sdk.init("https://986e4dcb75f04ca59cfe4fddb7cc3955@sentry.io/1519039")
# sentry_sdk.init(
#     "https://986e4dcb75f04ca59cfe4fddb7cc3955@sentry.io/1519039",
#     integrations=[FlaskIntegration()],
#     environment='staging'
# )

###############################################################################################################






