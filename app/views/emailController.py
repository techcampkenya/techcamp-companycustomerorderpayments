from flask import request,redirect,render_template,url_for,flash

from main import app
from views.decoratorsController import *
from models.email import EmailModel


# view email history
@app.route('/email/history')
@user_login_required
@emails_role_required
def email_history():
    if session:
        
        company_name = session['company_name']
        company_id = session['company_id']
        username = session['user_name']

        emails = EmailModel.get_email_byCompanyId(company_id)

    
        return render_template('email.html', user=username, companyName=company_name,emails=emails)
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))
