from flask import request,redirect,render_template,url_for,flash,session

from main import app
from views.decoratorsController import *
from models.crm import CRMModel,CRMSchema



@app.route('/tasks')
@user_login_required
def tasks():
    if session:
        
        company_name = session['company_name']
        company_id = session['company_id']
        username = session['user_name']

        crms = CRMModel.get_crms_by_companyId(company_id)
        # print("crmsss", crms)
        # serialize the result
        crms_schema = CRMSchema(many=True)
        json_crms = crms_schema.dump(crms)
        print(json_crms)

        return render_template('tasks.html',company=company_name, user=username,the_crms=json_crms)
    else:
        flash('Session has expired! Please sign in again', 'danger')
        return redirect(url_for('login'))
