from flask import request,redirect,render_template,url_for,flash,session
from main import app

from models.users import *
from controllers.companyController import Company
com1 = Company()


# company registration
@app.route('/registration', methods=['GET','POST'])
def registration():

    categories = ['Accounting & Tax Services',
            'Arts, Culture & Entertainment',
            'Auto Sales & Service',
            'Banking & Finance',
            'Business Services',
            'Community Organizations',
            'Education',
            'Health & Wellness',
            'Home Improvement',
            'Interior Design',
            'Insurance',
            'Internet & Web Services',
            'Lodging & Travel',
            'Shopping & Retail',
            'Transportation',
            'Wedding, Events & Meetings',
    ]

    if request.method == 'POST':
        if com1.register_company():
            flash('Successfully registered!', 'success')
            return redirect(url_for('dashboard'))
            # return redirect(url_for('login'))

    return render_template('register.html', cat=categories)



# login

# login page.
@app.route('/login', methods=['GET','POST'])
def login():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']

        # check if the email provided exists in the database
        if UserModel.check_email_exists(email):
            if UserModel.validate_user_password(email, password):
                status = UserModel.user_status(email)
                # print(status)
                # print(type(status))
                if status == 0 or status == 1:
                    session['logged_in'] = True
                    session['user_name'] = UserModel.get_user_fullName(email)
                    session['user_id'] = UserModel.get_user_id(email)
                    session['user_email'] = email
                    session['company_id'] = UserModel.get_user_company_id(email)
                    session['company_name'] = UserModel.get_user_company_name(email)
                    
                    # get the current logged in user!
                    the_user = UserModel.get_user_by_email(session['user_email'])
                    # get his roles
                    the_user_roles = []

                    for role in the_user.role:
                        the_user_roles.append(role.id)

                    session['roles'] = the_user_roles
                    # send_message("Account login",session['user_name'],session['user_id'],session['company_id'])
                    return redirect(url_for('dashboard'))
                else:
                    flash('Your account has been suspended! Contact Admin for support', 'danger')
                    return redirect(url_for('login'))
            else:
                flash('Incorrect login credentials', 'danger')
                return redirect(url_for('login'))
        else:
            flash('Incorrect login credentials', 'danger')
            return redirect(url_for('login'))

    return render_template('login.html')




@app.route('/logout', methods=['GET','POST'])
def logout():
    session.clear()
    # send_message("user logged out",username,session['user_id'],company_id)
    return redirect(url_for('login'))