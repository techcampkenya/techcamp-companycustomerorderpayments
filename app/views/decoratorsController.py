from flask import request,redirect,render_template,url_for,flash

from functools import wraps
from flask import session,flash,redirect,url_for,request

###############################################################################################################
""" THE DECORATORS"""

# create a login required wrapper for user
def user_login_required(f): #define a function whose first parameter is f, which is convention for the fact that it wraps a function
    @wraps(f)
    def wrap(*args,**kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash('Unauthorized! Please log in','danger')
            return redirect(url_for('login', next=request.url))
    return wrap


def customer_role_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 1 in session['roles'] or 2 in session['roles']:
            return f(*args, **kwargs)
        else:
            flash('You are not authorized to access this service! Contact admin','danger')
            return render_template('forbidden.html')
    return wrap

def orders_role_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 1 in session['roles'] or 3 in session['roles']:
            return f(*args, **kwargs)
        else:
            flash('You are not authorized to access this service! Contact admin','danger')
            return render_template('forbidden.html')
    return wrap

def emails_role_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 1 in session['roles'] or 4 in session['roles']:
            return f(*args, **kwargs)
        else:
            flash('You are not authorized to access this service! Contact admin','danger')
            return render_template('forbidden.html')
    return wrap

def payments_role_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 1 in session['roles'] or 5 in session['roles']:
            return f(*args, **kwargs)
        else:
            flash('You are not authorized to access this service! Contact admin','danger')
            return render_template('forbidden.html')
    return wrap

def income_expense_role_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 1 in session['roles'] or 5 in session['roles']:
            return f(*args, **kwargs)
        else:
            return render_template('forbidden.html')
    return wrap

def admin_role_required(f):
    @wraps(f)
    def wrap(*args,**kwargs):
        if 1 in session['roles']:
            return f(*args, **kwargs)
        else:
            flash('You are not authorized to access this service! Contact admin','danger')
            return render_template('forbidden.html')
    return wrap

################################################################################################################
################################################################################################################
