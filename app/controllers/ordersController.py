from flask import request, redirect, url_for, flash, session

from models.orders import OrdersModel


class Orders:

    # place an order
    def add_order(self):
        try:
            title = request.form['title']
            description = request.form['description']
            customer_id = request.form['customer_id']
            company_id = request.form['company_id']
            
            # send to db
            order = OrdersModel(title=title, description=description, customer_id=customer_id, company_id=company_id )
            order.insert_record()
            flash('Order has successfully been placed!', 'success')
            company_name = session['company_name']
            company_id = session['company_id']
            username = session['user_name']
            # send_message("added customer order",username,session['user_id'],company_id)
            # return redirect(url_for(''))
        except Exception as e:
            flash('Oops! an error occured', 'danger')
            print(e)


    # edit order details
    def edit_order_details(self,id):
        try:
            title = request.form['title']
            description = request.form['description']

            if OrdersModel.update_order_records(id=id,title=title,description=description):
                flash('Record has successfully been updated!', 'success')
                company_name = session['company_name']
                company_id = session['company_id']
                username = session['user_name']
                # send_message("edited order id {} record".format(id),username,session['user_id'],company_id)
            else:
                flash('Could not update record! Please try again', 'danger')


        except Exception as e:
            flash('An error occured while processing your request! Try again later')
            print(e)



    # get orders for a specific customer
    def get_customer_orders(self, cust_id):
        return OrdersModel.get_orders_by_customerId(cust_id)