from flask import request, redirect, url_for, flash

from models.items import ItemsModel

class Item:

    # add new item
    def add_item(self):
        try:
            title = request.form['title']
            description =request.form['description']
            quantity = request.form['qt']
            unit_cost = request.form['unit_cost']
            items_cost = int(quantity) * float(unit_cost)
            # print(items_cost)
            # picture =
            order_id = request.form['order_id']

            item = ItemsModel(title=title, description=description,quantity=quantity,unit_cost=unit_cost,item_cost=items_cost,order_id=order_id)
            item.insert_record()
            flash('Item has successfully been added!', 'success')
            company_name = session['company_name']
            company_id = session['company_id']
            username = session['user_name']
            # send_message("added order item",username,session['user_id'],company_id)
        except Exception as e:
            flash('Oops! an error occured', 'danger')
            print(e)