from flask import request,flash,redirect,url_for

from models.incategory import IncomeCategory
from models.income import IncomeModel

class Income:

    # ADD NEW INCOME CATEGORY
    def add_income_category(self):
        try:
            category = request.form['category']
            company_id = request.form['company_id']
            

            if IncomeCategory.check_category_exists(category):
                flash('category already exists', 'danger')
                return redirect(url_for('income'))
            else:
                incategory = IncomeCategory(category_title=category.title(), company_id=company_id)
                incategory.insert_record()
                flash('Category has successfully been added!', 'success')
                
        except Exception as e:
            print(e)


    # GET ALL INCOME CATEGORIES
    def get_all_income_categories(self):
        return IncomeCategory.fetch_all_records()

    # GET ALL INCOMES CATEGORIES IN A COMPANY
    def get_company_income_categories(self, id):
        return IncomeCategory.get_income_categories_by_id(id)
    
    
    # ADD AN INCOME
    def add_income(self):
        try:
            category = request.form['category']
            title = request.form['incometitle']
            amount = request.form['amount']
            # category_id = request.form['category']
            company_id = request.form['company_id']

            income = IncomeModel(title=title, amount=amount,category=category ,
                                company_id=company_id )
            income.insert_record()
            flash('Income has been successfully added!', 'success')
            company_id = session['company_id']
            company_name = session['company_name']
            user_name = session['user_name']
            # send_message("added income",username,session['user_id'],company_id)
            return redirect(url_for('income'))
        except Exception as e:
            print(e)


    # GET ALL INCOMES
    def get_all_incomes(self):
        return IncomeModel.fetch_all_records()

    # GET COMPANY INCOMES
    def get_company_incomes(cls, id):
        return IncomeModel.get_company_incomes(id)
    
    
    