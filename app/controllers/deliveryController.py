from flask import request, redirect, url_for, flash

from models.delivery import DeliveryModel

class Delivery:

    # add delivery
    def add_delivery(self):
        try:
            dperson = request.form['person']
            dfee = request.form['fee']
            dcountry = request.form['country']
            dcity = request.form['city']
            dstreet = request.form['street']
            order_id = request.form['order_id']

            delivery = DeliveryModel(delivery_person=dperson,delivery_fee=dfee,delivery_city=dcity,
                                    delivery_country=dcountry,delivery_street=dstreet,delivery_order_id=order_id)
            delivery.insert_record()
            flash('Success','success')
            print('apo sawa')

        except Exception as e:
            flash(str(e), 'danger')
            # print(e)

