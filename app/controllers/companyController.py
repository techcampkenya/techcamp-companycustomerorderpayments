from flask import request, flash, redirect,url_for,session
from werkzeug.security import generate_password_hash

from models.company import CompanyModel
from models.users import UserModel,RolesModel

from main import db

class Company:

    # register company
    def register_company(self):
        # try:
            company_name = request.form['cname']
            company_email = request.form['cemail']
            company_category = request.form['comcategory']
            registered_by = request.form['fname']
            email = request.form['cemail']
            password = request.form['pass']
            confirmpass = request.form['cpass']
            protected_pass = generate_password_hash(password)

            if password != confirmpass:
                flash('Passwords do not match! ', 'danger')
                return False


            if CompanyModel.check_company_name_exists(company_name):
                flash('Company already exists, contact support for more information', 'danger')
                return False


            if CompanyModel.check_company_email_exists(company_email):
                flash('Email already exists, contact support for more information', 'danger')
                return False
            
            
            company = CompanyModel(company_name=company_name.title(), company_email=company_email.lower(), registered_by=registered_by.title(), email=email, company_secret_pass=protected_pass,company_category=company_category)
            company.insert_record()

            x = UserModel(full_name=registered_by, email=company_email,company_id=company.id, password=protected_pass, status=1)
            recd = x.insert_record()

            
            # register the 
            super_admin = RolesModel.get_super_admin(1)
            recd.role.append(super_admin)
            db.session.commit()

            # super_admin.user.append(recd)
            # db.session.commit()

            session['logged_in'] = True
            session['user_name'] = UserModel.get_user_fullName(email)
            session['user_email'] = email
            session['company_id'] = UserModel.get_user_company_id(email)
            session['company_name'] = UserModel.get_user_company_name(email)

            # get the current logged in user!
            the_user = UserModel.get_user_by_email(email)
            # get his roles
            the_user_roles = []

            for role in the_user.role:
                print(role.id)
                the_user_roles.append(role.id)

            session['roles'] = the_user_roles
            
            return True
        # except Exception as e:
        #     return False
  

    # get all the companies
    def get_companies(self):
        return CompanyModel.query.all()





