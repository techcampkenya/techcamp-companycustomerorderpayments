from flask import request, redirect, url_for, flash,make_response,render_template,session
import pdfkit
import datetime
today = datetime.datetime.now().strftime("%Y-%m-%d")

from models.payment import PaymentModel
from models.orders import OrdersModel
from models.income import IncomeModel
from models.company import CompanyModel

class Payment:

    # add payment
    def add_payment(self):
        try:
            mode = request.form['mode']
            reference_no = request.form['refno']
            amount = request.form['amount']
            order_id = request.form['order_id']
            cid = request.form['cid']

            company = CompanyModel.query.filter_by(id=cid).first()
            cemail = company.company_email

            # send to db
            pay = PaymentModel(mode=mode, reference_number=reference_no, amount=amount, order_id=order_id,
                                company_id=cid )
            pay.insert_record()

            incom = IncomeModel(category='Payments',title="orderId-{}-payment".format(order_id),amount=amount,company_id=cid)
            incom.insert_record()

            # update the order status and payment status 
            OrdersModel.update_order_status(order_id)
            OrdersModel.update_payment_status(order_id)

            # GET THE CURRENT ORDER ITEMS TOTAL and THE CURRENT PAID AMOUNT OF THE ORDER
            order_total = OrdersModel.get_order_items_total(order_id)
            order_payments = PaymentModel.get_order_payments_total(order_id)
            

            if order_total - order_payments <= 0:
                OrdersModel.order_fully_paid(order_id)


            flash('Payment has successfully been updated!', 'success')
            company_name = session['company_name']
            company_id = session['company_id']
            username = session['user_name']
            # send_message("added crm information",username,session['user_id'],company_id)
            # return response
        except Exception as e:
            flash('Oops! an error occured', 'danger')
            print(e)

    