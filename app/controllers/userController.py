from flask import request, render_template, redirect, url_for, flash, session
from werkzeug.security import generate_password_hash
import random

from models.users import UserModel
from models.company import CompanyModel

class User:

    # def add user
    def register_user(self):
        try:
            full_name = request.form['fname']
            email = request.form['email']
            company_id = request.form['company_id']
            password = random.randint(1000,9999)

            print("Your password is",password)
            protected_pass = generate_password_hash(str(password),salt_length=10)

            # get company name
            company = CompanyModel.get_company_by_id(company_id)
            company_name = company.company_name
            
            if UserModel.check_email_exists(email):
                flash('The email is already in use!', 'danger')
                return redirect(url_for('user_registration'))
            else:

                user = UserModel(full_name=full_name.title(), email=email.lower(),company_id=company_id, password=protected_pass)
                user.insert_record()
                
                from configs.configs import send_simple_email
                password_template = render_template('sendpassword.html',generated_pass=password,name=company_name)
                # send_simple_email(email,"Account Set Up",password_template,company_id)
                flash('User successfully created and Login password successfully sent to their email!', 'success')
                # logs
                company_id = session['company_id']
                company_name = session['company_name']
                user_name = session['user_name']
                # send_message("added new user",username,session['user_id'],company_id)
                return redirect(url_for('user_roles'))
                
            return password
        except Exception as e:
            flash('Error!', 'danger')
            print(e)

    # def edit user details
    def edit_user_details(self,id):
        try:
            full_name = request.form['fname']
            email = request.form['email']

            if UserModel.update_user_details(id=id,full_name=full_name,email=email):
                flash('Record has successfully been deleted','success')
                # send_message("edited user details",username,session['user_id'],company_id)
                company_id = session['company_id']
                company_name = session['company_name']
                user_name = session['user_name']
            else:
                flash('An error occured while processing your request! Try again later','danger')

        except Exception as e:
            flash('An error occured while processing your request! Try again later','danger')
            print(e)


    
    

