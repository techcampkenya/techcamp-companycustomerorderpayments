from flask import request, redirect, url_for, flash

from models.materials import MaterialsModel

class Material:

    # add material
    def add_material(self):
        try:
            title = request.form['title']
            description = request.form['description']
            order_id = request.form['order_id']
            
            # send to db
            material = MaterialsModel(title=title, description=description, order_id=order_id)
            material.insert_record()
            flash('Material has successfully been added', 'success')
        except Exception as e:
            flash('Oops! an error occured', 'danger')
            print(e)