from flask import request, flash,redirect,url_for

from models.expcategory import ExpenditureCategory
from models.expenditure import ExpenditureModel

class Expenditure:
    
    # add new expenditure category
    def add_expenditure_category(self):
        try:
            category_name = request.form['category']
            company_id = request.form['company_id']

            if ExpenditureCategory.check_category_exists(category_name):
                flash('category already exists!', 'danger')
                return redirect(url_for('expenditure'))
            else:
                category = ExpenditureCategory(category_title=category_name.title(), company_id=company_id)
                category.insert_record()
                flash('category has successfully been added!', 'success')
                return redirect(url_for('expenditure'))
        except Exception as e:
            print(e)


    # get all expenditure categories
    def get_all_expenditure_categories(self):
        return ExpenditureCategory.fetch_all_records()

    # get all expenditures by company id
    def get_exp_categories_by_company(self,id):
        return ExpenditureCategory.get_expenditure_categories_by_company_id(id)

    # add an expenditure
    def add_expenditure(self):
        try:
            category = request.form['category']
            title = request.form['expensetitle']
            amount = request.form['amount']
            # category_id = request.form['category']
            company_id = request.form['company_id']

            expense = ExpenditureModel(category=category,title=title,amount=amount,
                                    company_id=company_id)
            expense.insert_record()
            flash('Expenditure has successfully been added', 'success')
            company_name = session['company_name']
            company_id = session['company_id']
            username = session['user_name']
        except Exception as e:
            print(e)


    # get all expenditures
    def get_all_expenditures(self):
        return ExpenditureModel.fetch_all_records()

    # get all expenditures by company
    def get_company_expenditures(self, id):
        return ExpenditureModel.get_company_expenditures(id)

