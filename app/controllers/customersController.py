from flask import request, redirect, url_for, flash,session
# from producer.producer import send_message
from models.customers import CustomerModel

class Customer:

    # register customer
    def register_customer(self):
        try:
            full_name = request.form['fullname']
            email = request.form['email']
            phone_number = request.form['phonenumber']
            gender = request.form['gender']
            company_id = request.form['company_id']


            customer = CustomerModel(full_name=full_name,email=email,phone_number=phone_number,gender=gender,
                                        company_id=company_id)

            if CustomerModel.check_customer_exists_in_company(email=email,cid=company_id):
                flash('customer email already exists!', 'danger')
            else:
                company_name = session['company_name']
                company_id = session['company_id']
                username = session['user_name']
                customer.insert_record()
                # send_message("added new customer",username,session['user_id'],company_id)
                flash('customer successfully added!', 'success')
            
        except Exception as e:
            flash(str(e), 'danger')
            


    # edit customer records
    def update_customer_records(self,id):
        try:
            full_name = request.form['fullname']
            email = request.form['email']
            phone_number = request.form['phonenumber']
            gender = request.form['gender']

            if CustomerModel.update_record(id=id,fullname=full_name,email=email,phone=phone_number,gender=gender):
                flash('Records successfully updated','success')
                company_name = session['company_name']
                company_id = session['company_id']
                username = session['user_name']
                # send_message("updated customer record ",username,session['user_id'],company_id)
            else:
                flash("Error!",'danger')

        except Exception as e:
            print(e)
            flash("error!",'danger')


    # delete customer records
    def delete_customer_record(self,id):
        try:
            delete = CustomerModel.delete_customer_record(id)
            if delete:
                flash('Record has successfully been deleted!')
        except Exception as e:
            print(e)
            


    # get all the customers
    def get_all_customers(self):
        return CustomerModel.fetch_all_records()

    