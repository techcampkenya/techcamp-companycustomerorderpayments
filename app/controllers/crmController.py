from flask import request, redirect, url_for, flash,session

# from models.crm import CRMModel
from models.crm import CRMModel

# url = 'http://127.0.0.1:5000'
url = 'http://139.59.155.134:4000'

class CRM:

    # add crm
    def add_crm(self):
        try:
            description = request.form['description']
            rating = request.form['rating']
            next_contact_date = request.form['next_date']
            order_id = request.form['order_id']
            company_id = request.form['company_id']
            customer_name = request.form['customer_name']
            
            if len(next_contact_date) == 0:
                next_contact_date = None

            
            # send to db
            crm = CRMModel(title="contact {}".format(customer_name),description=description,rating=rating,
                            start=next_contact_date,order_id=order_id,company_id=company_id,
                            url="{}/order/{}/management".format(url,order_id)) #

            crm.insert_record()
            flash('Conversation has successfully been updated!', 'success')
            company_name = session['company_name']
            company_id = session['company_id']
            username = session['user_name']
            # send_message("added crm information",username,session['user_id'],company_id)
        except Exception as e:
            flash('Oops! an error occured', 'danger')
            print(e)