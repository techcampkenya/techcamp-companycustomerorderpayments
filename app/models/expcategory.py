from models.common import CommonMethods, db

class ExpenditureCategory(db.Model, CommonMethods):
    __tablename__ = 'expenditure_categories'
    __bind_key__ = 'expenditure'
    id = db.Column(db.Integer, primary_key=True)
    category_title = db.Column(db.String(55), nullable=False)
    company_id = db.Column(db.Integer,nullable=False)
    # expenses = db.relationship('ExpenditureModel', backref='expcategory', lazy=True)
    
    # check if category exists
    @classmethod
    def check_category_exists(cls, name):
        record = cls.query.filter_by(category_title=name).first()
        if record:
            return True
        else:
            return False

    # get all expenditure categories by company_id
    @classmethod
    def get_expenditure_categories_by_company_id(cls, id):
        records = cls.query.filter_by(company_id=id).all()
        return records