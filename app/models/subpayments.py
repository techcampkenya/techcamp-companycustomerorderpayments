from models.common import db, CommonMethods
from sqlalchemy.sql import func

class SubscriptionPayments(db.Model,CommonMethods):
    __tablename__ = 'subpayments'
    __bindkey__ = 'subscription'
    id = db.Column(db.Integer, primary_key=True)
    company_id = db.Column(db.Integer, nullable=False)
    payment_id = db.Column(db.Integer, nullable=False)
    payment_type = db.Column(db.Integer, nullable=False)
    amount = db.Column(db.Float, nullable=False)
    ssid = db.Column(db.Integer, nullable=False)
    created_at =  db.Column(db.DateTime(timezone=True), default=func.now())