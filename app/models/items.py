from models.common import db, CommonMethods
from sqlalchemy.sql import func
from main import ma

class ItemsModel(db.Model, CommonMethods):
    __tablename__ = 'items'
    __bind_key__ = 'orders'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(), nullable=False)
    description = db.Column(db.String(), nullable=False)
    company_id = db.Column(db.Integer, nullable=False)
    user_id = db.Column(db.Integer, nullable=False)
    unit_cost = db.Column(db.Float, nullable=False)
    picture_url = db.Column(db.String(), nullable=True)
    created_on =  db.Column(db.DateTime(timezone=True), default=func.now())
    
    
    # get items by order_id
    @classmethod
    def get_items_by_orderId(cls,order_id):
        return cls.query.filter_by(order_id=id).all()

    # get items by the company_id
    @classmethod
    def get_items_by_companyId(cls,company_id):
        return cls.query.filter_by(company_id=company_id).all()

class ItemSchema(ma.Schema):
    class Meta:
        fields = ('id','title','description','company_id','user_id','unit_cost','created_ons')

items_schema = ItemSchema(many=True)    