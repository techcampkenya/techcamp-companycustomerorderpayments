from models.common import CommonMethods, db
from werkzeug.security import check_password_hash
from sqlalchemy.sql import func

class CompanyModel(CommonMethods, db.Model):
    __tablename__ = 'companies'
    __bind_key__ = 'company'
    id = db.Column(db.Integer, primary_key=True)
    company_name = db.Column(db.String(), nullable=False, unique=True)
    company_email = db.Column(db.String(), nullable=False, unique=True)
    registered_by = db.Column(db.String(), nullable=False)
    image_url = db.Column(db.String(), nullable=True)
    email = db.Column(db.String(), nullable=False)
    company_category = db.Column(db.String(), nullable=False)
    verified = db.Column(db.Integer, nullable=True)
    company_secret_pass = db.Column(db.String(), nullable=False)
    users = db.relationship('UserModel', backref='company', lazy=True)
    # inventories = db.relationship('InventoryModel', backref='company', lazy=True)
    created_on =  db.Column(db.DateTime(timezone=True), default=func.now())


    # check if the company email
    @classmethod
    def check_company_email_exists(cls,email):
        company = cls.query.filter_by(company_email=email).first()
        if company:
            return True
        else:
            return False

    # check if the company name
    @classmethod
    def check_company_name_exists(cls,name):
        company = cls.query.filter_by(company_name=name).first()
        if company:
            return True
        else:
            return False

    # validate company secret pass
    @classmethod
    def validate_company_secret_pass(cls,email,passw):
        company = cls.query.filter_by(company_email=email).first()
        if company and check_password_hash(company.company_secret_pass, passw):
            return True
        else:
            return False

    # get company id
    @classmethod
    def get_company_id(cls,email):
        return cls.query.filter_by(company_email=email).first().id

    # get company by id
    @classmethod
    def get_company_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

    #get company name
    @classmethod
    def get_company_name(cls,email):
        return cls.query.filter_by(company_email=email).first().company_name

    #get company registered by
    @classmethod
    def get_company_owner(cls,email):
        return cls.query.filter_by(company_email=email).first().registered_by 

    
