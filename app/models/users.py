from models.common import CommonMethods, db
from werkzeug.security import check_password_hash
from sqlalchemy.sql import func
from main import ma


role_user = db.Table('role_user',
        db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
        db.Column('role_id', db.Integer, db.ForeignKey('roles.id')),
        info={'bind_key': 'company'}
)

class UserModel(db.Model,CommonMethods):
    __tablename__ = 'users'
    __bind_key__ = 'company'
    id = db.Column(db.Integer, primary_key=True)
    full_name = db.Column(db.String(), nullable=False)
    email = db.Column(db.String(), nullable=False)
    status = db.Column(db.Integer, default='0', nullable=False) #0- active and not changed password, 1- active and changed password 2-suspended
    password = db.Column(db.String(), nullable=False)
    company_id = db.Column(db.Integer, db.ForeignKey('companies.id'), nullable=False)
    created_on =  db.Column(db.DateTime(timezone=True), default=func.now())
    

    # update user records
    @classmethod
    def update_user_details(cls,id,full_name=None,email=None):
        record = cls.query.filter_by(id=id).first()
        if record:
            if full_name:
                record.full_name = full_name
            if email:
                record.email = email

            db.session.commit()
            return True
        else:
            return False


    # get the user by email
    @classmethod
    def get_user_by_email(cls,email):
        return cls.query.filter_by(email=email).first()

    # get user by id
    @classmethod
    def get_user_byID(cls,id):
        return cls.query.filter_by(id=id).first()
    
    # check if a email exists
    @classmethod
    def check_email_exists(cls, email):
        record = cls.query.filter_by(email=email).first()
        if record:
            return True
        else:
            return False

    # validate the password
    @classmethod
    def validate_user_password(cls, email, password):
        record = cls.query.filter_by(email=email).first()
        # validate the password
        if record and check_password_hash(record.password, password):
            return True
        else:
            return False

    # get a user company id
    @classmethod
    def get_user_company_id(cls, email):
         return cls.query.filter_by(email=email).first().company_id

    # get a user company name
    @classmethod
    def get_user_company_name(cls, email):
        return cls.query.filter_by(email=email).first().company.company_name


    # get a user's id
    @classmethod
    def get_user_id(cls, email):
        return cls.query.filter_by(email=email).first().id
       
    # get a user full name
    @classmethod
    def get_user_fullName(cls, email):
        return cls.query.filter_by(email=email).first().full_name

    # get user role
    @classmethod
    def get_user_role(cls,email):
        return cls.query.filter_by(email=email).first().role

    # get the user status
    @classmethod
    def user_status(cls,email):
        return cls.query.filter_by(email=email).first().status

    # suspend a user
    @classmethod
    def suspend_user(cls, id):
        user = cls.query.filter_by(id=id).first()
        if user:
            user.status = 2
            db.session.commit()
            return True
        else:
            return False

    # reactivate user
    @classmethod
    def reactivate_user(cls, id):
        user = cls.query.filter_by(id=id).first()
        if user:
            user.status = 1
            db.session.commit()
            return True
        else:
            return False
        

class RolesModel(db.Model):
    __tablename__ = 'roles'
    __bind_key__ = 'company'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(), nullable=False)
    description = db.Column(db.String(), nullable=False)
    users = db.relationship('UserModel', secondary=role_user, backref='role')

    # create
    def create_record(self):
        db.session.add(self)
        db.session.commit()
        return self

    # select * from roles
    @classmethod
    def fetch_records(cls):
        return cls.query.all()

    # get role 1
    @classmethod
    def get_super_admin(cls,id):
        return cls.query.filter_by(id=id).first()

    # get role by id
    @classmethod
    def get_role_byID(cls,id):
        return cls.query.filter_by(id=id).first()


class RoleSchema(ma.Schema):
    class Meta:
        fields = ("id","name", "description")