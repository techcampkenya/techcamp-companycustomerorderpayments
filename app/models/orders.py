from models.common import CommonMethods, db
from datetime import datetime
from sqlalchemy.sql import func



class OrdersModel(db.Model, CommonMethods):
    __tablename__ = 'orders'
    __bind_key__ = 'orders'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(55), nullable=False)
    description = db.Column(db.String(), nullable=False)
    order_status = db.Column(db.Integer, default='0') # 0 - is a booking # 1 - is an order
    payment_status = db.Column(db.Integer, default='0')# 0 - not paid, # 1-partially paid, #2-fully paid
    quality_check_status = db.Column(db.Integer, default='0') # 0-not checked, # 1 - failed #2 - passed
    delivery_status = db.Column(db.Integer, default='0') # 0 -not delivered, 1-in transit 2-delivered
    order_date = db.Column(db.DateTime(timezone=True), default=func.now())
    company_id = db.Column(db.Integer, nullable=False)
    customer_id = db.Column(db.Integer, nullable=False)
    # items = db.relationship('ItemsModel', backref='order_details', lazy=True)
    crms = db.relationship('CRMModel', backref='order', lazy=True)
    materials = db.relationship('MaterialsModel', backref='theorder', lazy=True)
    deliveries = db.relationship('DeliveryModel', backref='thaorder', lazy=True, uselist=False)
    order_details = db.relationship('OrderDetails', backref='thaaorder', lazy=True)


    # update order details
    @classmethod
    def update_order_records(cls, id, title=None,description=None):
        record = cls.query.filter_by(id=id).first()
        if record:
            if title:
                record.title = title
            if description:
                record.description = description

            db.session.commit()
            return True
        else:
            return False

    # get orders by company id
    @classmethod
    def get_orders_byCompanyId(cls,id):
        return cls.query.filter_by(company_id=id).all()

    # change delivery status to in transit
    @classmethod
    def update_status_to_intransit(cls,id):
        record = cls.query.filter_by(id=id).first()
        if record:
            if record.delivery_status != 1:
                record.delivery_status = 1
                db.session.commit()
                return True
        else:
            return False


    # update status to delivered
    @classmethod
    def update_status_to_delivered(cls,id):
        record = cls.query.filter_by(id=id).first()
        if record:
            if record.delivery_status != 2:
                record.delivery_status = 2
                db.session.commit()
                return True
        else:
            return False


    # get order items
    @classmethod
    def get_items(self, id):
        order = OrdersModel.query.filter_by(id=id).first()
        return order.order_details


    # get an order by id
    @classmethod
    def get_order_byId(cls, id):
        return cls.query.filter_by(id=id).first()

    # get all orders associate to a customer id
    @classmethod
    def get_orders_by_customerId(cls, id):
        return cls.query.filter_by(customer_id=id).all()
    
    # get a user associated to an order
    @classmethod
    def get_customerid(cls,id):
        return cls.query.filter_by(id=id).first().customer_id

    # update order status to 1
    @classmethod
    def update_order_status(cls, id):
        order = cls.query.filter_by(id=id).first()
        # get the order
        if order:
            # if order status is 0 at first, update the the order status to 1
            if order.order_status == 0:
                order.order_status = 1
                db.session.commit()
                
                return True
        else:
            return False

    # update payment status to 1
    @classmethod
    def update_payment_status(cls, id):
        order = cls.query.filter_by(id=id).first()
        # get the order
        if order:
            # if order status is 0 at first, update the the order status to 1
            if order.payment_status == 0:
                order.payment_status = 1
                db.session.commit()
                
                return True
        else:
            return False

    # update order payment to 2 ie fully paid
    @classmethod
    def order_fully_paid(cls,id):
        order = cls.query.filter_by(id=id).first()
        # get the order
        if order:
            # if order status is 0 at first, update the the order status to 1
            if order.payment_status == 0 or 1:
                order.payment_status = 2
                db.session.commit()
                return True
        else:
            return False


    # get order items total
    @classmethod
    def get_order_items_total(cls,id):
        order = cls.query.filter_by(id=id).first()
        order_items = order.order_details
        order_items_total = map(lambda x:x.sub_total,order_items)
        return sum(list(order_items_total))