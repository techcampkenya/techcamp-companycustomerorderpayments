from models.common import CommonMethods, db

class IncomeCategory(db.Model, CommonMethods):
    __tablename__ = 'income_categories'
    __bind_key__ = 'income'
    id = db.Column(db.Integer, primary_key=True)
    category_title = db.Column(db.String(), nullable=False)
    company_id = db.Column(db.Integer,nullable=False)
    # incomes = db.relationship('IncomeModel', backref='incategory', lazy=True)


    # check if a category already exists
    @classmethod
    def check_category_exists(cls, name):
        record = cls.query.filter_by(category_title=name).first()
        if record:
            return True
        else:
            return False

    # get categories by id
    @classmethod
    def get_income_categories_by_id(cls,id):
        records = cls.query.filter_by(company_id=id).all()
        return records

