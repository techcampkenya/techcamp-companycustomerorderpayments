from models.common import db, CommonMethods
from sqlalchemy.sql import func

class MaterialsModel(db.Model, CommonMethods):
    __tablename__ = 'materials'
    __bind_key__ = 'orders'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(), nullable=False)
    description = db.Column(db.String(), nullable=False)
    created_on =  db.Column(db.DateTime(timezone=True), default=func.now())
    order_id = db.Column(db.Integer, db.ForeignKey('orders.id'), nullable=False)
    
    

    