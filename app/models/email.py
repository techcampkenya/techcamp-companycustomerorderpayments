from models.common import CommonMethods,db
from sqlalchemy.sql import func

class EmailModel(db.Model,CommonMethods):
    __tablename__ = 'emails'
    __bind_key__ = 'email'
    id = db.Column(db.Integer, primary_key=True)
    company_id = db.Column(db.Integer, nullable=False)
    user_id = db.Column(db.Integer, nullable=False)
    customer_id = db.Column(db.Integer, nullable=False)
    subject = db.Column(db.String(), nullable=False)
    content = db.Column(db.String(), nullable=False)
    created_on =  db.Column(db.DateTime(timezone=True), default=func.now())
    email_type = db.Column(db.Integer, nullable=False)
    delivery_status = db.Column(db.Integer, nullable=False)
    read_status = db.Column(db.Integer, nullable=False)
    # tag = db.Column(db.String(), nullable=True)

    # fetch email by company-id
    @classmethod
    def get_email_byCompanyId(cls,company_id):
        return cls.query.filter_by(company_id=company_id).all()