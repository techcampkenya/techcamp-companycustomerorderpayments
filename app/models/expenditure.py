from models.common import CommonMethods, db
from sqlalchemy.sql import func

class ExpenditureModel(CommonMethods, db.Model):
    __tablename__ = 'expenditures'
    __bind_key__ = 'expenditure'
    id = db.Column(db.Integer, primary_key=True)
    category = db.Column(db.String(), nullable=False)
    title = db.Column(db.String(55), nullable=False )
    amount = db.Column(db.Float, nullable=False)
    created_on =  db.Column(db.DateTime(timezone=True), default=func.now())
    company_id = db.Column(db.Integer, nullable=False)
    # excid = db.Column(db.Integer, db.ForeignKey('expenditure_categories.id'))

    # get all expenditures by a company
    @classmethod
    def get_company_expenditures(cls,id):
        records = cls.query.filter_by(company_id=id).all()
        return records