from models.common import CommonMethods, db
from sqlalchemy.sql import func

class IncomeModel(db.Model, CommonMethods):
    __tablename__ = 'incomes'
    __bind_key__ = 'income'
    id = db.Column(db.Integer, primary_key=True)
    category = db.Column(db.String(), nullable=False)
    title = db.Column(db.String(), nullable=False)
    amount = db.Column(db.Float, nullable=False)
    created_on =  db.Column(db.DateTime(timezone=True), default=func.now())
    company_id = db.Column(db.Integer, nullable=False)
    # incomecid = db.Column(db.Integer, db.ForeignKey('income_categories.id'), nullable=True)

    # get a company incomes
    @classmethod
    def get_company_incomes(cls, id):
        records = cls.query.filter_by(company_id=id).all()
        return records

    