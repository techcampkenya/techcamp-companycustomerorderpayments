from models.common import db, CommonMethods
from sqlalchemy.sql import func

class SubWallet(db.Model, CommonMethods):
    __tablename__ = 'subwallet'
    __bindkey__ = 'subscription'
    id = db.Column(db.Integer,primary_key=True)
    exp_date = db.Column(db.DateTime, nullable=False)
    starr_date = db.Column(db.DateTime, nullable=True)
    created_at =  db.Column(db.DateTime(timezone=True), default=func.now())
    