from main import db

class CommonMethods:

    # insert record
    def insert_record(self):
        db.session.add(self)
        db.session.commit()
        return self

    # fetch all records
    @classmethod
    def fetch_all_records(cls):
        return cls.query.all()