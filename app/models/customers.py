from models.common import CommonMethods, db
from sqlalchemy.sql import func

class CustomerModel(db.Model, CommonMethods):
    __tablename__ = 'customers'
    __bind_key__ = 'customers'
    id = db.Column(db.Integer, primary_key=True)
    full_name = db.Column(db.String(55), nullable=False)
    email = db.Column(db.String(55), nullable=False)
    created_on =  db.Column(db.DateTime(timezone=True), default=func.now())
    phone_number = db.Column(db.String(55), nullable=False)
    gender = db.Column(db.String(55), nullable=False)
    company_id = db.Column(db.Integer, nullable=False)

    # edit customer records
    @classmethod
    def update_record(cls, id, fullname=None,email=None, phone=None, gender=None):
        record = cls.query.filter_by(id=id).first()
        if record:
            if fullname:
                record.full_name = fullname
            if email:
                record.email = email
            if phone:
                record.phone_number = phone
            if gender:
                record.gender = gender

            db.session.commit()
            return True
        else:
            return False


    # delete customer records
    @classmethod
    def delete_customer_record(cls,id):
        record = cls.query.filter_by(id=id)
        if record.first():
            record.delete()
            db.session.commit()
            return True
        else:
            return False



    # get all customer by company id
    @classmethod
    def get_customer_by_company(cls,id):
        return cls.query.filter_by(company_id=id).all()
    
    # check if customer exists
    @classmethod
    def check_email_exists(cls, email):
        record = cls.query.filter_by(email=email).first()
        if record:
            return True
        else:
            return False

    # get a customer by id
    @classmethod
    def get_customer_by_id(cls,id):
        return cls.query.filter_by(id=id).first()

    # 
    @classmethod
    def check_customer_exists_in_company(cls,email,cid):
        record = cls.query.filter_by(email=email).first()
        # print("type",type(cid))
        try:
            x = (record.company_id == int(cid))
            return x
        except:
            return False

