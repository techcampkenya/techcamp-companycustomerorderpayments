from models.common import CommonMethods, db
from sqlalchemy.sql import func

class DeliveryModel(CommonMethods,db.Model):
    __tablename__ = 'deliveries'
    __bind_key__ = 'orders'
    id = db.Column(db.Integer, primary_key=True)
    created_on =  db.Column(db.DateTime(timezone=True), default=func.now())
    delivery_fee = db.Column(db.Float,nullable=False)
    delivery_city = db.Column(db.String(), nullable=False)
    delivery_country = db.Column(db.String(), nullable=False)
    delivery_person = db.Column(db.String(), nullable=False)
    delivery_street = db.Column(db.String(), nullable=False)
    delivery_order_id = db.Column(db.Integer, db.ForeignKey('orders.id'))