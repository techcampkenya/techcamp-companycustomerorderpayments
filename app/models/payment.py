from models.common import db, CommonMethods
from sqlalchemy.sql import func

class PaymentModel(db.Model, CommonMethods):
    __tablename__ = 'payments'
    __bind_key__ = 'payments'
    id = db.Column(db.Integer, primary_key=True)
    mode = db.Column(db.String(), nullable=False)
    reference_number = db.Column(db.String(), nullable=False, )
    amount = db.Column(db.Float, nullable=False)
    created_on =  db.Column(db.DateTime(timezone=True), default=func.now())
    order_id = db.Column(db.Integer, nullable=False)
    company_id = db.Column(db.Integer, nullable=False)

    
    # get payments by order_id
    @classmethod
    def get_payments_by_orderId(cls,id):
        return cls.query.filter_by(order_id=id).all() 

    # get payments by company id
    @classmethod
    def get_payment_by_companyId(cls,id):
        return cls.query.filter_by(company_id=id).all()

    # get order payments
    @classmethod
    def get_order_payments_total(cls,id):
        payments = cls.query.filter_by(order_id=id).all()
        total_amount = map(lambda x:x.amount,payments)
        return sum(list(total_amount))