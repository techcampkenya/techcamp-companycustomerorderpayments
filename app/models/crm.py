from models.common import db, CommonMethods
from sqlalchemy.sql import func, column
from main import ma


class CRMModel(db.Model, CommonMethods):
    __tablename__ = 'crms'
    __bind_key__ = 'orders'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(), nullable=False)
    description = db.Column(db.String(), nullable=False)
    rating = db.Column(db.String(), nullable=False)
    created_on =  db.Column(db.DateTime(timezone=True), default=func.now())
    start = db.Column(db.String() ,nullable=True) #next_contact_date
    order_id = db.Column(db.Integer, db.ForeignKey('orders.id'), nullable=False)
    company_id = db.Column(db.Integer, nullable=False)
    url = db.Column(db.String(), nullable=False)
    status = db.Column(db.Integer, nullable=False, default=0) #0 -incomplete, 1-complete
    

    # get crms by company id
    @classmethod
    def get_crms_by_companyId(cls,cid):
        return cls.query.filter(CRMModel.start !=None).filter_by(company_id=cid,status=0).all()


    # update the task status
    @classmethod
    def update_crm_to_complete(cls,cid):
        record = cls.query.filter_by(id=cid).first()
        if record.status != 1:
            record.status = 1
            db.session.commit()


class CRMSchema(ma.Schema):
    class Meta:
        fields = ("title", "start", "url")