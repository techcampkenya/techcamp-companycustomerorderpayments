from models.common import CommonMethods,db
from sqlalchemy.sql import func
from main import ma

class OrderDetails(db.Model,CommonMethods):
    __tablename__ = 'order_details'
    __bind_key__ = 'orders'
    id = db.Column(db.Integer, primary_key=True)
    order_id = db.Column(db.Integer,db.ForeignKey('orders.id'), nullable=False)
    item_id = db.Column(db.Integer, nullable=False)
    company_id = db.Column(db.Integer, nullable=False)
    quantity = db.Column(db.Float, nullable=False)
    sub_total = db.Column(db.Float, nullable=False)
    created_on =  db.Column(db.DateTime(timezone=True), default=func.now())

class OrderSchema(ma.Schema):
    class Meta:
        fields = ("id", "order_id","item_id","company_id","quantity","sub_total","created_on")

od_schema = OrderSchema()

