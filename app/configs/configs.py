
class Config(object):
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class Development(Config):
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEBUG = True
    SECRET_KEY = 'some-secret-key'
    SQLALCHEMY_BINDS = {
        'customers': 'postgresql://postgres:Brian8053@@127.0.0.1:5432/varicciCustomers',
        'orders': 'postgresql://postgres:Brian8053@@127.0.0.1:5432/varicciOrders',
        'payments': 'postgresql://postgres:Brian8053@@127.0.0.1:5432/varicciPayments',
        'income': 'postgresql://postgres:Brian8053@@127.0.0.1:5432/appIncomeDatabase',
        'expenditure': 'postgresql://postgres:Brian8053@@127.0.0.1:5432/appExpenditureDatabase',
        'company': 'postgresql://postgres:Brian8053@@127.0.0.1:5432/appCompanyDatabase',
        'email': 'postgresql://postgres:Brian8053@@127.0.0.1:5432/appEmailDatabase',
        # 'subscription': 'postgresql://postgres:Brian8053@@127.0.0.1:5432/omssubscription'
    }

class Staging(Config):
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEBUG = True
    SECRET_KEY = 'some-secret-key'
    SQLALCHEMY_BINDS = {
        'customers': 'postgresql://myuser:mypass@172.17.0.1:5432/omscustomers',
        'orders': 'postgresql://myuser:mypass@172.17.0.1:5432/omsorders',
        'payments': 'postgresql://myuser:mypass@172.17.0.1:5432/omspayments',
        'income': 'postgresql://myuser:mypass@172.17.0.1:5432/omsincome',
        'expenditure': 'postgresql://myuser:mypass@172.17.0.1:5432/omsexpenditure',
        'company': 'postgresql://myuser:mypass@172.17.0.1:5432/omscompany',
        'email': 'postgresql://myuser:mypass@172.17.0.1:5432/appemaildatabase',
        # 'subscription': 'postgresql://myuser:mypass@172.17.0.1:5432/omssubscription'
    }

class Production(Config):
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEBUG = True
    SECRET_KEY = 'some-secret-key'
    SQLALCHEMY_BINDS = {
        'customers': 'postgresql://postgres:mypass@172.17.0.1:5432/omscustomers',
        'orders': 'postgresql://postgres:mypass@172.17.0.1:5432/omsorders',
        'payments': 'postgresql://postgres:mypass@172.17.0.1:5432/omspayments',
        'income': 'postgresql://postgres:mypass@172.17.0.1:5432/omsincome',
        'expenditure': 'postgresql://postgres:mypass@172.17.0.1:5432/omsexpenditure',
        'company': 'postgresql://postgres:mypass@172.17.0.1:5432/omscompany',
        'email': 'postgresql://postgres:mypass@172.17.0.1:5432/appemaildatabase',
        # 'subscription': 'postgresql://myuser:mypass@172.17.0.1:5432/omssubscription'
    }


import requests
def send_simple_email(to,subject,message,company_id):
    sending = requests.post(
        "https://api.mailgun.net/v3/mail.bizweb.co.ke/messages",
        auth=("api", "key-5e692b718d18a403b13c1be83279d5e2"),
        data={"from": "Bizweb ERP <info@mail.bizweb.co.ke>",
              "to": to,
              "o:tag" : company_id,
              "subject": subject,
              "html": message})
    return sending.json()



