# import pika
# import json
# from datetime import datetime

# connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
# channel = connection.channel()

# """
# Were connected now, to a broker on the local machine - hence the localhost. 
# If we wanted to connect to a broker on a different machine wed simply specify its name or IP address here.
# """

# """
# -Create a recipient 
# -When RabbitMQ quits or crashes it will forget the queues and messages unless you tell it not to
# -make sure that messages aren't lost: we need to mark both the queue and messages as durable
# """
# channel.queue_declare(queue='logsBizweb',durable=True)


# """
# -In RabbitMQ a message can never be sent directly to the queue, it always needs to go through an EXCHANGE.
# -use a default exchange identified by an empty string.
# -NB:allows us to specify exactly to which queue the message should go.
# -The queue name needs to be specified in the routing_key parameter:
# """
# def send_message(event,user,user_id,companyId):
#     # create a dictionary to send the message to the queue
#     to_json ={
#         "message":event,
#         "user":user,
#         'user_id':user_id,
#         "company_id":companyId
#     }
#     # NB:before sending Serialize dict to a JSON formatted str
#     """
#     Now we need to mark our messages as persistent - by supplying a delivery_mode property with a value 2.
#     """
#     # print(" [x] Sent {}".format(json.dumps(to_json)))

#     # while True:
#     channel.basic_publish(exchange='',routing_key='logsBizweb',body=json.dumps(to_json))
#         # connection.process_data_events(60)

#     # connection.close()