# Host / User / Password

## Development
localhost

## Staging
172.17.0.1:5432
user: myuser
password: mypass

## Production

# SQL Query
create database omscompany;
create database omsexpenditure;
create database omsincome;
create database omsorders;
create database omspayments;
create database omscustomers;
create database appemaildatabase;

## create roles
make a POST request to http://164.90.231.59:5000/bizweb/roles

ROLES (add them in the following order)
    Super Admin
    Customers
    Orders
    Emails
    Payments
    income_expense

payload
    {
	    "name":"Super Admin",
	    "description":"Super Admin"
    }


## docker 
docker-compose up -d

## main.py
- toggle between development and production config ( line 10-14)

